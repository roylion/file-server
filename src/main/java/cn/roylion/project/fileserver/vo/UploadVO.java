package cn.roylion.project.fileserver.vo;

import cn.roylion.project.fileserver.util.enums.StatusCodeEnum;

/**
 * @Author: Roylion
 * @Description:
 * @Date: Created in 15:54 2018/10/29
 */
public class UploadVO {

    private String code;
    private String message;
    private Object data;

    public UploadVO(String code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public UploadVO(StatusCodeEnum status, Object data) {
        this.code = status.getCode();
        this.message = status.getMessage();
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
