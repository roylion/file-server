package cn.roylion.project.fileserver.util.enums;

/**
 * @Author: Roylion
 * @Description:
 * @Date: Created in 14:01 2018/10/30
 */
public enum StatusCodeEnum {

    SUCCESS("1001", "success"),
    FAILD("1002", "faild"),
    FILE_NOT_EXISTS("1003", "file not exists"),
    PARAMS_INVALID("1004", "params invalid");

    private String code;
    private String message;

    StatusCodeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
