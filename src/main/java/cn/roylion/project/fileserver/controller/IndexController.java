package cn.roylion.project.fileserver.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Author: Roylion
 * @Description:
 * @Date: Created in 16:35 2018/10/31
 */
@Controller
public class IndexController {

    @GetMapping("hello")
    @ResponseBody
    public String hello() {
        return "hello file-server";
    }

}
