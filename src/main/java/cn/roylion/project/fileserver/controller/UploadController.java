package cn.roylion.project.fileserver.controller;

import cn.roylion.project.fileserver.exception.BaseException;
import cn.roylion.project.fileserver.util.enums.StatusCodeEnum;
import cn.roylion.project.fileserver.vo.UploadVO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author: Roylion
 * @Description:
 * @Date: Created in 14:33 2018/10/29
 */
@Controller
public class UploadController {

    @Value("${uploadFilePath}")
    private String uploadFilePath;
    @Value("${filePrefix}")
    private String filePrefix;

    @ExceptionHandler(BaseException.class)
    @ResponseBody
    public UploadVO exceptionHandler(BaseException e) {
        return new UploadVO(e.getCode(), e.getMessage(), null);
    }

    @RequestMapping("upload")
    @ResponseBody
    public UploadVO upload(MultipartFile file) {

        // 参数校验
        if (file == null || file.isEmpty()) {
            throw new BaseException(StatusCodeEnum.PARAMS_INVALID);
        }

        // 获取后缀
        String suffix = file.getOriginalFilename().replaceAll(".*\\.(.*)", ".$1");
        // 生成保存路径
        String date = new SimpleDateFormat("yyyyMMdd/").format(new Date());
        String lastFilePath = uploadFilePath + date;
        File lastFile = new File(lastFilePath);
        if (!lastFile.exists()) {
            lastFile.mkdirs();
        }

        try {
            // 生成文件名
            String fileName = md5HashCode(file.getInputStream()) + suffix;
            File dest = new File(lastFilePath + fileName);
            file.transferTo(dest);
            Map data = new HashMap();
            data.put("filePath", filePrefix + date + fileName);

            return new UploadVO(StatusCodeEnum.SUCCESS, data);
        } catch (IOException e) {
            throw new BaseException(StatusCodeEnum.FAILD);
        }
    }

    @RequestMapping("/file/**")
    @ResponseBody
    public void view(@RequestParam(required = false, defaultValue = "0") int d, HttpServletRequest req, HttpServletResponse res) {
        // 获取文件真实路径
        String filePath = req.getRequestURI();
        filePath = filePath.replaceAll("/file/(.*)", uploadFilePath + "$1");

        File file = new File(filePath);
        boolean download = d == 1;
        try (ServletOutputStream out = res.getOutputStream();
             FileInputStream fis = new FileInputStream(file)) {
            if (download) {
                // 获取文件名
                String fileName = filePath.replaceAll(".*/(.*)", "$1");
                res.setHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");
                res.setContentType("application/octet-stream");
                res.setContentLengthLong(file.length());
            }

            byte[] io = new byte[1024 * 10];
            int len = 0;
            while ((len = fis.read(io)) != -1) {
                out.write(io, 0, len);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * java获取文件的md5值
     *
     * @param fis 输入流
     * @return
     */
    public static String md5HashCode(InputStream fis) {
        try {
            //拿到一个MD5转换器,如果想使用SHA-1或SHA-256，则传入SHA-1,SHA-256
            MessageDigest md = MessageDigest.getInstance("MD5");

            //分多次将一个文件读入，对于大型文件而言，比较推荐这种方式，占用内存比较少。
            byte[] buffer = new byte[1024];
            int length = -1;
            while ((length = fis.read(buffer, 0, 1024)) != -1) {
                md.update(buffer, 0, length);
            }
            fis.close();
            //转换并返回包含16个元素字节数组,返回数值范围为-128到127
            byte[] md5Bytes = md.digest();
            BigInteger bigInt = new BigInteger(1, md5Bytes);//1代表绝对值
            return bigInt.toString(16);//转换为16进制
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * java计算文件32位md5值
     *
     * @param fis 输入流
     * @return
     */
    public static String md5HashCode32(InputStream fis) {
        try {
            //拿到一个MD5转换器,如果想使用SHA-1或SHA-256，则传入SHA-1,SHA-256
            MessageDigest md = MessageDigest.getInstance("MD5");

            //分多次将一个文件读入，对于大型文件而言，比较推荐这种方式，占用内存比较少。
            byte[] buffer = new byte[1024];
            int length = -1;
            while ((length = fis.read(buffer, 0, 1024)) != -1) {
                md.update(buffer, 0, length);
            }
            fis.close();

            //转换并返回包含16个元素字节数组,返回数值范围为-128到127
            byte[] md5Bytes = md.digest();
            StringBuffer hexValue = new StringBuffer();
            for (int i = 0; i < md5Bytes.length; i++) {
                int val = ((int) md5Bytes[i]) & 0xff;//解释参见最下方
                if (val < 16) {
                    /**
                     * 如果小于16，那么val值的16进制形式必然为一位，
                     * 因为十进制0,1...9,10,11,12,13,14,15 对应的 16进制为 0,1...9,a,b,c,d,e,f;
                     * 此处高位补0。
                     */
                    hexValue.append("0");
                }
                //这里借助了Integer类的方法实现16进制的转换
                hexValue.append(Integer.toHexString(val));
            }
            return hexValue.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

}

