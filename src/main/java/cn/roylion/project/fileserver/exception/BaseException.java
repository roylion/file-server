package cn.roylion.project.fileserver.exception;

import cn.roylion.project.fileserver.util.enums.StatusCodeEnum;

/**
 * @Author: Roylion
 * @Description:
 * @Date: Created in 15:56 2018/10/29
 */
public class BaseException extends RuntimeException {

    private String code;

    public BaseException(String code, String message) {
        super(message);
        this.code = code;
    }

    public BaseException(StatusCodeEnum status) {
        super(status.getMessage());
        this.code = status.getCode();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
